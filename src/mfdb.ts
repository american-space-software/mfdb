import { IndexSearch, IndexService, SortDirection } from "./service/index-service"
import { ColumnDefinition, DatabaseDefinition, TableDefinition } from "./dto/definition"
import { Transaction } from "./dto/transaction"

const { SortedMap } = require('immutable-sorted')

const stringify = require('fast-json-stable-stringify')
const all = require('it-all')

const uint8ArrayToString = require('uint8arrays/to-string')
const uint8ArrayConcat = require('uint8arrays/concat')

import CID from "cids"
import { IOService } from "./service/io-service"
import { DefinitionService } from "./service/definition-service"
import { runInThisContext } from "vm"

class MFdb {

    public selectedDatabase: string
    private activeTransaction: Transaction

    private indexService: IndexService
    private ioService: IOService
    private definitionService:DefinitionService

    constructor(
        private ipfs
    ) {
        this.ioService = new IOService(this.ipfs)
        this.definitionService = new DefinitionService(this.ioService, this.ipfs)
        this.indexService = new IndexService(this.definitionService, this.ioService, this.ipfs, this)
    }

    /**
     * Create a new database from the passed in DatabaseDefinition. Will throw exceptions if there
     * are problems creating the database.
     * @param definition 
     */
    async createDatabase(definition: DatabaseDefinition) {

        if (definition.name == undefined) {
            throw Error("Must give database a name")
        }

        //Make sure it doesn't already exist
        if (await this.ioService.databaseExists(definition.name)) {
            throw Error("Database already exists")
        }

        //Create the directory
        console.debug(`Creating database ${definition.name} at ${this.ioService.getDatabasePath(definition.name)}`)
        await this.ipfs.files.mkdir(this.ioService.getDatabasePath(definition.name), {
            parents: true
        })

        //Validate the tables
        for (let tableDefinition of definition.tables) {
            this.definitionService.validateTableDefinition(tableDefinition)
        }

        //Save schema
        await this.definitionService.saveDefinition(definition)

    }

    /**
     * Drop a database from your local MFS. Does not delete underlying data.
     * @param name 
     */
    async dropDatabase(name: string) {

        //Remove directory
        try {
            await this.ipfs.files.rm(this.ioService.getDatabasePath(name), {
                recursive: true
            })
        } catch (ex) {}
    }

    async databaseExists(database: string) : Promise<boolean> {
        return this.ioService.databaseExists(database)
    }

    /**
     * Use the database with this name. All queries, inserts, updates, and deletes will use the tables in
     * this database.
     * @param name 
     */
    async useDatabase(name: string) {

        console.log(`Using database ${name}`)
        this.selectedDatabase = name

        //Clear state
        delete this.activeTransaction

        await this.definitionService.loadDefinition(name)
        await this.indexService.clearCache()

    }

    /**
     * Starts a new transaction. A transaction must be started before calling functions that change data.
     */
    startTransaction() {
        console.debug('Starting new transaction')
        this.activeTransaction = {
            changes: [],
            tables: []
        }
    }

    /**
     * Writes all changes to IPFS that have been called since we called startTransaction()
     */
    async commit() {

        console.debug(`Commiting ${this.activeTransaction.changes.length} changes to: ${this.activeTransaction.tables}`)

        //Process changes
        for (let change of this.activeTransaction.changes) {
            await change()
        }

        //Flush maps
        for (let table of this.activeTransaction.tables) {
            await this.indexService.flushIndexes(table)
        }

        delete this.activeTransaction

    }

    /**
     * Queues a change to insert a record into a table.
     * @param table 
     * @param value 
     */
    insert(table:string, value: any) {

        if (!this.selectedDatabase) throw Error("No database selected")
        if (!this.activeTransaction) throw Error("No transaction started")

        this.queueChange(table, async () => {
            await this.insertValue(table, value)
        })

    }


    /**
     * Does the actual work of inserting a record.
     * @param table 
     * @param value 
     */
    private async insertValue(table:string, value:any) {
        
        //Get primary key field
        let primaryKey:ColumnDefinition = await this.definitionService.getPrimaryKeyDefinition(table)

        //Make sure we get passed a primary key
        if (value[primaryKey.name] === undefined) throw Error(`Insert failed: Record does not contain primary key field ${primaryKey.name}`)

        //Validate that the record doesn't exist otherwise throw an exception
        let existing = await this.get(table, value[primaryKey.name])
        if (existing) {
            throw Error(`Insert failed: Primary key ${value[primaryKey.name]} already exists in ${table} table`)
        }

        //Save value           
        let cid = await this.ioService.write(value)

        //Get definition
        let tableDefinition:TableDefinition = await this.definitionService.getTableDefinition(table)

        //Update indexes
        for (let column of tableDefinition.columns) {
            
            await this.indexService.updateIndex({
                table: table,
                index: column.name,
                mapKey: column.primary ? value[primaryKey.name] : value[column.name],
                mapValue: column.primary ? cid.toString() : value[primaryKey.name]
            })
                        
        }
    }

    /**
     * Update a record in a table. Matches on primary key. 
     * @param table 
     * @param value 
     */
    update(table:string, value:any) {

        if (!this.selectedDatabase) throw Error("No database selected")
        if (!this.activeTransaction) throw Error("No transaction started")

        this.queueChange(table, async () => {
            await this.updateValue(table, value)

        })

    }

    /**
     * Does the actual work of inserting a record.
     * @param table 
     * @param value 
     */
    private async updateValue(table:string, value:any) {
        
        //Get primary key field
        let primaryKey:ColumnDefinition = await this.definitionService.getPrimaryKeyDefinition(table)

        //Make sure we get passed a primary key
        if (value[primaryKey.name] === undefined) throw Error(`Update failed: Record does not contain primary key field ${primaryKey.name}`)

        //Get existing value
        let existing = await this.get(table, value[primaryKey.name])

        //Validate that the record doesn't exist otherwise throw an exception
        if (existing === undefined) {
            throw Error(`Update failed: Primary key ${value[primaryKey.name]} does not exist in ${table} table`)
        }
        
        //Save value           
        let cid = await this.ioService.write(value)

        //Get definition
        let tableDefinition:TableDefinition = await this.definitionService.getTableDefinition(table)

        //Update indexes
        for (let column of tableDefinition.columns) {
            
            await this.indexService.updateIndex({
                table: table,
                index: column.name,
                mapKey: column.primary ? value[primaryKey.name] : value[column.name],
                mapExistingKey: column.primary ? value[primaryKey.name] : existing[column.name],
                mapValue: column.primary ? cid.toString() : value[primaryKey.name]
            })
                        
        }

    }


    put(table:string, value:any) {

        if (!this.selectedDatabase) throw Error("No database selected")
        if (!this.activeTransaction) throw Error("No transaction started")

        this.queueChange(table, async () => {

            //Get primary key field
            let primaryKey:ColumnDefinition = await this.definitionService.getPrimaryKeyDefinition(table)

            //Make sure we get passed a primary key
            if (value[primaryKey.name] === undefined) throw Error(`Update failed: Record does not contain primary key field ${primaryKey.name}`)

            //Get existing value
            let existing = await this.get(table, value[primaryKey.name])

            if (existing) {
                await this.updateValue(table, value)
            } else {
                await this.insertValue(table, value)
            }

        })




    }

    /**
     * Returns the CID for a record in a table. Key must match a value in the primary key of the table. 
     * @param table 
     * @param key 
     */
    async getCID(table: string, key: any): Promise<CID> {

        if (!this.selectedDatabase) throw Error("No database selected")

        let primaryKeyDefinition = await this.definitionService.getPrimaryKeyDefinition(table)
        let primaryIndexMap = await this.indexService.getIndexMap(table,primaryKeyDefinition.name)

        let cid = primaryIndexMap.get(key.toString())

        return cid

    }

    /**
     * Returns a record with the passed primary key in the named table. 
     * @param table 
     * @param key 
     */
    async get(table: string, key: any): Promise<any> {

        if (!this.selectedDatabase) throw Error("No database selected")

        let cid = await this.getCID(table, key)

        if (cid) {
            let value = await this.ioService.read(cid.toString())
            return value
        }

    }

    /**
     * Searches an index in a table for rows. Takes an IndexSearch object.
     * @param indexSearch 
     */
    async searchIndex(indexSearch:IndexSearch) {

        let schema = await this.definitionService.getTableDefinition(indexSearch.table)
        let definition = schema.columns.filter(column => column.name == indexSearch.index)[0]

        let indexMap

        //Get the index they asked for or just use the primary key index. 
        if (indexSearch.index) {
            indexMap = await this.indexService.getIndexMap(indexSearch.table, indexSearch.index)
            definition = schema.columns.filter(column => column.name == indexSearch.index)[0]
        } else {
            let pkDefinition:ColumnDefinition = this.definitionService.getPrimaryKeyDefinition(indexSearch.table)
            indexMap = await this.indexService.getIndexMap(indexSearch.table, pkDefinition.name)
            definition = schema.columns.filter(column => column.name == pkDefinition.name)[0]
        }



        //Todo: put some validation here
        
        if (!schema || !definition || !indexMap) return []

        let results = []
        let primaryKeys = []

        if (indexSearch.value) {

            if (definition.unique) {

                let primaryKey = indexMap.get(indexSearch.value)
                if (primaryKey) primaryKeys.push(primaryKey)
                
            } else {

                let list = indexMap.get(indexSearch.value)

                if (list) {
                    for (let primaryKey of list) {
                        primaryKeys.push(primaryKey)
                    }
                }

            }

        } else {

            primaryKeys = Object.keys(indexMap.toSeq().toJS())
        }

        //Sort
        primaryKeys.sort()
        if (indexSearch.sortDirection == SortDirection.ASC) primaryKeys.reverse()

        //Look up actual values
        let count = 0

        for (let primaryKey of primaryKeys) {

            if (results.length >= indexSearch.limit) break

            if (count < indexSearch.offset) {
                count++
                continue
            } else {
                count++
            }
            
            results.push(await this.get(indexSearch.table, primaryKey))

        }

        return results

    }

    /**
     * Counts the records in a table. 
     * @param table 
     */
    async count(table: string) {

        if (!this.selectedDatabase) throw Error("No database selected")

        let primaryKeyDefinition = await this.definitionService.getPrimaryKeyDefinition(table)
        let primaryIndexMap = await this.indexService.getIndexMap(table,primaryKeyDefinition.name)

        return primaryIndexMap.size

    }

    /**
     * Delete a record from a table. Also removes record from any indexes. 
     * @param table 
     * @param key 
     */
    delete(table: string, key: any) {

        if (!this.selectedDatabase) throw Error("No database selected")
        if (!this.activeTransaction) throw Error("No transaction started")

        this.queueChange(table, async () => {

            //Get primary key field
            let primaryKey:ColumnDefinition = await this.definitionService.getPrimaryKeyDefinition(table)

            //Get existing value
            let existing = await this.get(table, key)

            //Get schema
            let schema = await this.definitionService.getTableDefinition(table)

            //Remove from all indexes
            for (let column of schema.columns) {
                // console.debug(`Updating ${indexName} index in ${table}`)

                await this.indexService.updateIndex({
                    table: table,
                    index: column.name,
                    mapKey: undefined,
                    mapExistingKey: existing[column.name],
                    mapValue: key,

                })

                // await this.indexService.updateIndex(table, column.name, key, undefined, existing ? existing[column.name] : undefined)
            }

        })

    }

    /**
     * Flush data to MFS.
     */
    async flushToMFS() {
        
        console.debug(`Flushing database '${this.selectedDatabase}' to MFS`)

        let definition:DatabaseDefinition = await this.definitionService.activeDefinition

        for (let table of definition.tables) {

            let primaryKeyDefinition = await this.definitionService.getPrimaryKeyDefinition(table.name)
            let primaryIndexMap = await this.indexService.getIndexMap(table.name, primaryKeyDefinition.name)

            for (let entry of primaryIndexMap.entries()) {

                let id = entry[0]
                let cid = entry[1]

                let path = `${this.ioService.getTableDirectoryName(this.selectedDatabase, table.name)}/${id}`

                if (!await this.ioService.fileMatches(path, cid)) {
                    console.log(`Copying ${cid} to ${path}`)

                    if (await this.ioService.fileExists(path)) {
                        await this.ioService.removeFile(path)
                    }

                    await this.ipfs.files.cp(`/ipfs/${cid}`, path)
                }


            }

        }

    }

    // async flushToJSON() {

    //     console.debug(`Flushing database '${this.selectedDatabase}' to JSON`)

    //     let definition:DatabaseDefinition = await this.definitionService.activeDefinition

    //     for (let table of definition.tables) {

    //         let primaryKeyDefinition = await this.definitionService.getPrimaryKeyDefinition(table.name)
    //         let primaryIndexMap = await this.indexService.getIndexMap(table.name, primaryKeyDefinition.name)

    //         for (let entry of primaryIndexMap.entries()) {

    //             let id = entry[0]
    //             let cid = entry[1]

    //             let path = `${this.ioService.getTableDirectoryName(this.selectedDatabase, table.name)}/${id}`

    //             if (!await this.ioService.fileMatches(path, cid)) {
    //                 console.log(`Copying ${cid} to ${path}`)

    //                 await this.ipfs.files.cp(`/ipfs/${cid}`, path)
    //             }


    //         }

    //     }

    // }


    /**
     * Returns the CID of the MFS directory where the database is stored. 
     * @param name 
     */
    async getDatabaseCID(name: string) {

        if (!this.ioService.databaseExists(name)) {
            throw Error("Database does not exist")
        }

        let stat = await this.ipfs.files.stat(this.ioService.getDatabasePath(name))

        return stat.cid

    }

    /**
     * Mount an MFS directory by hash. 
     * @param name 
     * @param cid 
     */
    async updateFromCid(name: string, cid: string) {

        console.log(`Updating ${name} from ${cid}`)

        let path = this.ioService.getDatabasePath(name)

        let existingCid
            
        if (await this.ioService.fileExists(path)) {
            existingCid = await this.getDatabaseCID(name)
        }


        if (existingCid?.toString() == cid) {

            console.log(`Database ${name} already up to date with ${cid}`)
            await this.useDatabase(name)

            return 

        } else {

            //Move existing
            if (await this.ioService.databaseExists(name)) {
                console.log(`Moving ${path} to /mfdb-bak/${name}`)
                await this.ipfs.files.mv(path, `/mfdb-bak/${name}`, {
                    parents: true
                })
            }
    
    
            //Copy to right spot
            console.log(`Copying ${cid}`)
            await this.ipfs.files.cp(`/ipfs/${cid}`, path, {
                parents: true
            })


            await this.useDatabase(name)
        }


        

    }

    private queueChange(table: string, f: Function) {

        //Add to changes
        this.activeTransaction.changes.push(f)

        //Add table
        let existing = this.activeTransaction.tables.filter(t => t == table)
        if (existing.length == 0) this.activeTransaction.tables.push(table)

    }






}

export {
    MFdb
}