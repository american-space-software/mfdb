interface Transaction {
    changes:Function[]
    tables:string[]
}

export {
    Transaction
}