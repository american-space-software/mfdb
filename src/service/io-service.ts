const toBuffer = require('it-to-buffer')
const stringify = require('fast-json-stable-stringify')
const all = require('it-all')
const uint8ArrayFromString = require('uint8arrays/from-string')
const uint8ArrayToString = require('uint8arrays/to-string')
const uint8ArrayConcat = require('uint8arrays/concat')

import CID from "cids"

class IOService {

    static MFS_DB = "mfdb"

    constructor(
        private ipfs
    ) {}

    async databaseExists(database: string) {
        return this.fileExists(this.getDatabasePath(database))
    }

    async tableExists(database: string, table:string ) {
        return this.fileExists(this.getTableDirectoryName(database, table))
    }

    async rowExists(database: string, table:string, primaryKey:string ) {
        return this.fileExists(`${this.getTableDirectoryName(database, table)}/${primaryKey}`)
    }




    public getDatabasePath(name: string) {
        return `/${IOService.MFS_DB}/${name}`
    }

    public getTableDirectoryName(database: string, table: string) {
        return `${this.getDatabasePath(database)}/tables/${table}`
    }

    public async fileExists(path: string) {
        try {
            await this.ipfs.files.stat(path)
            return true
        } catch (ex) {
            return false
        }
    }

    public async fileMatches(path:string, cid:string) {

        try {
            let stat = await this.ipfs.files.stat(path)
            
            if (stat.cid == cid) {
                return true
            }

        } catch (ex) {}

        return false

    }


    public async getFileContent(filename) {
        let bufferedContents = await toBuffer(this.ipfs.files.read(filename))  // a buffer
        let content = bufferedContents.toString()
        return JSON.parse(content)
    }

    public async write(value:any) : Promise<CID>{

        // console.debug(`Writing value to MFS at ${path}`)

        const result = await this.ipfs.add({
            content: stringify(value)
        })

        return result.cid

    }





    public async read(cid:string) {
        const files = await all(this.ipfs.get(cid))
        return JSON.parse(uint8ArrayToString(uint8ArrayConcat(await all(files[0].content))))
    }

    public async removeFile(path:string) {
        if (await this.fileExists(path)) {
            await this.ipfs.files.rm(path)
        }
    }



}

export {
    IOService
}