import { IOService } from "./io-service"
import { DefinitionService } from "./definition-service"
import { ColumnDefinition, TableDefinition } from "../dto/definition"
import { MFdb } from "../mfdb"

const stringify = require('fast-json-stable-stringify')
const { SortedMap } = require('immutable-sorted')


class IndexService {

    private indexMaps

    constructor(
        private definitionService:DefinitionService,
        private ioService:IOService,
        private ipfs,
        private mfdb:MFdb
    ) {}

    public cacheIndexMap(table:string, index:string, indexMap) {
        //Initialize 
        if (!this.indexMaps[table]) this.indexMaps[table] = {}
        this.indexMaps[table][index] = indexMap
    }

    public getCachedIndexMap(table:string, index:string) {
        if (!this.indexMaps || !this.indexMaps[table]) return
        return this.indexMaps[table][index]
    }



    public async updateIndex(indexUpdate:IndexUpdate) {

        let indexMap = await this.getIndexMap(indexUpdate.table, indexUpdate.index)
        const definition = await this.getDefinitionForIndex(indexUpdate.table, indexUpdate.index)

        if (!indexMap || !definition) return

        if (definition.unique) {

            if (indexUpdate.mapKey) {
                indexMap = indexMap.set(indexUpdate.mapKey.toString(), indexUpdate.mapValue)
            } else {

                if (indexUpdate.mapExistingKey) {
                    indexMap = indexMap.delete(indexUpdate.mapExistingKey.toString())
                }

            }

        } else {

            //Otherwise we're storing a list of values. Append this to it.

            let isNew: boolean = (Boolean(!indexUpdate.mapExistingKey) && Boolean(indexUpdate.mapKey))
            let isChanged: boolean = (!isNew && (indexUpdate.mapKey != indexUpdate.mapExistingKey))

            if (indexUpdate.mapExistingKey && isChanged) {
                //Remove from current list
                let currentList = indexMap.get(indexUpdate.mapExistingKey.toString())

                let currentIndex = currentList.indexOf(indexUpdate.mapValue)

                if (currentIndex >= 0) {
                    currentList.splice(currentIndex, 1)
                    indexMap = indexMap.set(indexUpdate.mapExistingKey, currentList)
                }

            }

            //If there's an actual value then insert it
            if (indexUpdate.mapKey && (isChanged || isNew)) {
                let currentList = indexMap.get(indexUpdate.mapKey.toString())

                if (!currentList) {
                    currentList = []
                    indexMap = indexMap.set(indexUpdate.mapKey.toString(), currentList)
                }

                currentList.push(indexUpdate.mapValue)

            }

        }

        this.cacheIndexMap(indexUpdate.table, indexUpdate.index, indexMap)

    }

    public async flushIndexes(table: string) {

        //Get schema
        let schema = await this.definitionService.getTableDefinition(table)

        for (let column of schema.columns) {

            let filename = `${this.ioService.getTableDirectoryName(this.mfdb.selectedDatabase, table)}/indexMaps/${column.name}`
            let indexMap = this.getCachedIndexMap(table, column.name)

            if (indexMap) {

                if (await this.ioService.fileExists(filename)) {
                    await this.ipfs.files.rm(filename, {
                        flush: true
                    })
                }

                // console.debug(`Writing indexMap to MFS at ${filename}`)

                let json = indexMap.toJSON()

                await this.ipfs.files.write(filename, stringify(json), {
                    create: true,
                    parents: true,
                    flush: true
                })
            }

        }

    }

    private async getDefinitionForIndex(table: string, index: string) {
        let definition: TableDefinition = await this.definitionService.getTableDefinition(table)
        return definition.columns.filter( column => column.name == index)[0]
    }

    public async getIndexMap(table: string, index: string) {

        //Check if it's cached
        let cached = this.getCachedIndexMap(table, index)
        if (cached) return cached

        //Load from MFS
        let indexMap

        try {
            let path = `${this.ioService.getTableDirectoryName(this.mfdb.selectedDatabase, table)}/indexMaps/${index}`

            await this.ipfs.files.stat(path)

            let loadedMap = await this.ioService.getFileContent(path)

            indexMap = SortedMap(loadedMap)

        } catch (ex) {
            // console.log(ex)
        }

        //If it doesn't exist create one
        if (!indexMap) indexMap = SortedMap()

        //Cache it
        this.cacheIndexMap(table, index, indexMap)

        return indexMap

    }

    public clearCache() {
        this.indexMaps = {}
    }

    

} 

interface IndexUpdate {
    table:string 
    index:string 
    mapKey:any 
    mapExistingKey?:any 
    mapValue: any 
}

interface IndexSearch {
    table: string, 
    index?:string, 
    value?:any, 
    sortDirection:SortDirection  
    offset: number
    limit: number 
}

enum SortDirection {
    DESC = "desc",
    ASC = "asc"
}


export {
    IndexService, IndexSearch, SortDirection
}