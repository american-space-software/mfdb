import { table } from "console"
import { ColumnDefinition, DatabaseDefinition, TableDefinition } from "../dto/definition"
import { IOService } from "./io-service"

const stringify = require('fast-json-stable-stringify')


class DefinitionService {

    static DEFINITION_FILENAME = "definition.json"

    public activeDefinition:DatabaseDefinition

    constructor(
        private ioService:IOService,
        private ipfs
    ) {}


    public async saveDefinition(definition:DatabaseDefinition) {
        
        let path = `${this.ioService.getDatabasePath(definition.name)}/definition/${DefinitionService.DEFINITION_FILENAME}`

        console.debug(`Saving definition for database ${definition.name} (${path})`)

        //Save 
        await this.ipfs.files.write(path, stringify(definition), {
            create: true,
            parents: true,
            flush: true
        })
    }


    public async loadDefinition(database:string) {
        let path = `${this.ioService.getDatabasePath(database)}/definition/${DefinitionService.DEFINITION_FILENAME}`
        this.activeDefinition = await this.ioService.getFileContent(path)
    }


    public validateTableDefinition(tableDefinition:TableDefinition) {

        let primaryKeyCount = 0

        for (let column of tableDefinition.columns) {
            if (column.primary) primaryKeyCount++
        }

        if (primaryKeyCount == 0) throw Error(`Invalid schema. Table ${tableDefinition.name} has no primary key.`)
        if (primaryKeyCount > 1) throw Error(`Invalid schema. Table ${tableDefinition.name} has multiple primary keys.`)

    }

    public getTableDefinition(table: string): TableDefinition {
        return this.activeDefinition.tables.filter( t => t.name == table)[0]
    }

    public getPrimaryKeyDefinition(table:string) : ColumnDefinition {
        let tableDefinition:TableDefinition = this.getTableDefinition(table)

        if (!tableDefinition) {
            throw Error(`No table definition found for ${table}`)
        }

        return tableDefinition.columns.filter( c => c.primary == true)[0]
    }



    public clearCache() {
        delete this.activeDefinition
    }


}

export {
    DefinitionService
}