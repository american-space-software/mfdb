const path = require('path')
import { merge } from 'webpack-merge'
const nodeExternals = require('webpack-node-externals')


const babelLoader = {
  loader: 'babel-loader',
  options: {
    cacheDirectory: false,
    presets: ['@babel/preset-env']
  }
}


let web = {
  name: "web",
  entry: './src/index.ts',
  mode: "production",
  module: {
    rules: [
      {
        test: /\.js$/,
        use: [babelLoader]
      },
      {
        test: /\.tsx?$/,
        exclude: '/node_modules/',
        loader: [babelLoader, 'ts-loader']
      },
    ],
  },
  resolve: {
    extensions: ['*', '.js', '.jsx', '.tsx', '.ts'],
  },
  output: {
    filename: 'index-browser.js',
    libraryTarget: 'umd',
    globalObject: 'this',
    path: path.resolve(__dirname, 'dist'),
  }
}




let node = {
  name: "node",
  entry: './src/index.ts',
  target: "node",
  mode: "production",
  externals: [nodeExternals()],
  module: {
    rules: [
      {
        test: /\.js$/,
        use: [babelLoader]
      },
      {
        test: /\.tsx?$/,
        exclude: '/node_modules/',
        loader: [babelLoader, 'ts-loader']
      },
    ],
  },
  resolve: {
    extensions: ['*', '.js', '.jsx', '.tsx', '.ts'],
  },
  output: {
    filename: 'index-node.js',
    libraryTarget: 'umd',
    path: path.resolve(__dirname, 'dist'),
  }
}



export default [
  web,
  node
]
