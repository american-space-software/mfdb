import { Schema } from "./schema";
interface TableDefinition {
    database: string;
    name: string;
    schema: Schema;
}
export { TableDefinition };
