import { IndexSearch } from "./service/index-service";
import { DatabaseDefinition } from "./dto/definition";
import CID from "cids";
declare class MFdb {
    private ipfs;
    selectedDatabase: string;
    private activeTransaction;
    private indexService;
    private ioService;
    private definitionService;
    constructor(ipfs: any);
    /**
     * Create a new database from the passed in DatabaseDefinition. Will throw exceptions if there
     * are problems creating the database.
     * @param definition
     */
    createDatabase(definition: DatabaseDefinition): Promise<void>;
    /**
     * Drop a database from your local MFS. Does not delete underlying data.
     * @param name
     */
    dropDatabase(name: string): Promise<void>;
    databaseExists(database: string): Promise<boolean>;
    /**
     * Use the database with this name. All queries, inserts, updates, and deletes will use the tables in
     * this database.
     * @param name
     */
    useDatabase(name: string): Promise<void>;
    /**
     * Starts a new transaction. A transaction must be started before calling functions that change data.
     */
    startTransaction(): void;
    /**
     * Writes all changes to IPFS that have been called since we called startTransaction()
     */
    commit(): Promise<void>;
    /**
     * Queues a change to insert a record into a table.
     * @param table
     * @param value
     */
    insert(table: string, value: any): void;
    /**
     * Does the actual work of inserting a record.
     * @param table
     * @param value
     */
    private insertValue;
    /**
     * Update a record in a table. Matches on primary key.
     * @param table
     * @param value
     */
    update(table: string, value: any): void;
    /**
     * Does the actual work of inserting a record.
     * @param table
     * @param value
     */
    private updateValue;
    put(table: string, value: any): void;
    /**
     * Returns the CID for a record in a table. Key must match a value in the primary key of the table.
     * @param table
     * @param key
     */
    getCID(table: string, key: any): Promise<CID>;
    /**
     * Returns a record with the passed primary key in the named table.
     * @param table
     * @param key
     */
    get(table: string, key: any): Promise<any>;
    /**
     * Searches an index in a table for rows. Takes an IndexSearch object.
     * @param indexSearch
     */
    searchIndex(indexSearch: IndexSearch): Promise<any[]>;
    /**
     * Counts the records in a table.
     * @param table
     */
    count(table: string): Promise<any>;
    /**
     * Delete a record from a table. Also removes record from any indexes.
     * @param table
     * @param key
     */
    delete(table: string, key: any): void;
    /**
     * Flush data to MFS.
     */
    flushToMFS(): Promise<void>;
    /**
     * Returns the CID of the MFS directory where the database is stored.
     * @param name
     */
    getDatabaseCID(name: string): Promise<any>;
    /**
     * Mount an MFS directory by hash.
     * @param name
     * @param cid
     */
    updateFromCid(name: string, cid: string): Promise<void>;
    private queueChange;
}
export { MFdb };
