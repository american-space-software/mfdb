interface DatabaseDefinition {
    name: string;
    tables: TableDefinition[];
}
interface TableDefinition {
    name: string;
    columns: ColumnDefinition[];
}
interface ColumnDefinition {
    name: string;
    primary?: boolean;
    unique: boolean;
}
export { DatabaseDefinition, ColumnDefinition, TableDefinition };
