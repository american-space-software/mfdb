interface Schema {
    columns: ColumnDefinition[];
}
interface ColumnDefinition {
    name: string;
    primary?: boolean;
    unique: boolean;
}
export { Schema, ColumnDefinition };
