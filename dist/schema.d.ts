interface Schema {
    [key: string]: {
        unique: boolean;
    };
}
export { Schema };
