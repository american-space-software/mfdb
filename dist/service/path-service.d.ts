declare class PathService {
    private ipfs;
    static MFS_DB: string;
    constructor(ipfs: any);
    getDatabasePath(name: string): string;
    getTableDirectoryName(database: string, table: string): string;
    fileExists(path: string): Promise<boolean>;
    getFileContent(filename: any): Promise<any>;
}
export { PathService };
