import { ColumnDefinition, DatabaseDefinition, TableDefinition } from "../dto/definition";
import { IOService } from "./io-service";
declare class DefinitionService {
    private ioService;
    private ipfs;
    static DEFINITION_FILENAME: string;
    activeDefinition: DatabaseDefinition;
    constructor(ioService: IOService, ipfs: any);
    saveDefinition(definition: DatabaseDefinition): Promise<void>;
    loadDefinition(database: string): Promise<void>;
    validateTableDefinition(tableDefinition: TableDefinition): void;
    getTableDefinition(table: string): TableDefinition;
    getPrimaryKeyDefinition(table: string): ColumnDefinition;
    clearCache(): void;
}
export { DefinitionService };
