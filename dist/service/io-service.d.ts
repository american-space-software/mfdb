import CID from "cids";
declare class IOService {
    private ipfs;
    static MFS_DB: string;
    constructor(ipfs: any);
    databaseExists(database: string): Promise<boolean>;
    tableExists(database: string, table: string): Promise<boolean>;
    rowExists(database: string, table: string, primaryKey: string): Promise<boolean>;
    getDatabasePath(name: string): string;
    getTableDirectoryName(database: string, table: string): string;
    fileExists(path: string): Promise<boolean>;
    fileMatches(path: string, cid: string): Promise<boolean>;
    getFileContent(filename: any): Promise<any>;
    write(value: any): Promise<CID>;
    read(cid: string): Promise<any>;
    removeFile(path: string): Promise<void>;
}
export { IOService };
