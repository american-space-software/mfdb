import { IOService } from "./io-service";
import { DefinitionService } from "./definition-service";
import { MFdb } from "../mfdb";
declare class IndexService {
    private definitionService;
    private ioService;
    private ipfs;
    private mfdb;
    private indexMaps;
    constructor(definitionService: DefinitionService, ioService: IOService, ipfs: any, mfdb: MFdb);
    cacheIndexMap(table: string, index: string, indexMap: any): void;
    getCachedIndexMap(table: string, index: string): any;
    updateIndex(indexUpdate: IndexUpdate): Promise<void>;
    flushIndexes(table: string): Promise<void>;
    private getDefinitionForIndex;
    getIndexMap(table: string, index: string): Promise<any>;
    clearCache(): void;
}
interface IndexUpdate {
    table: string;
    index: string;
    mapKey: any;
    mapExistingKey?: any;
    mapValue: any;
}
interface IndexSearch {
    table: string;
    index?: string;
    value?: any;
    sortDirection: SortDirection;
    offset: number;
    limit: number;
}
declare enum SortDirection {
    DESC = "desc",
    ASC = "asc"
}
export { IndexService, IndexSearch, SortDirection };
