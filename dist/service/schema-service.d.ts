import { ColumnDefinition, Schema } from "../dto/schema";
import { TableDefinition } from "../dto/table-definition";
import { PathService } from "./path-service";
declare class SchemaService {
    private pathService;
    private ipfs;
    static SCHEMA_FILENAME: string;
    schemas: {};
    constructor(pathService: PathService, ipfs: any);
    saveSchema(definition: TableDefinition): Promise<void>;
    validateSchema(schema: Schema): void;
    getSchemaForTable(database: string, table: string): Promise<Schema>;
    getPrimaryKeyDefinition(database: string, table: string): Promise<ColumnDefinition>;
}
export { SchemaService };
