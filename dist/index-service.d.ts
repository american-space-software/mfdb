declare class IndexService {
    private indexMaps;
    cacheIndexMap(database: string, table: string, index: string, indexMap: any): void;
    getCachedIndexMap(database: string, table: string, index: string): any;
}
export { IndexService };
