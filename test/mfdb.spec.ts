import assert from 'assert'

import { DatabaseDefinition, MFdb, SortDirection } from "../src/index"
import { DefinitionService } from '../src/service/definition-service'

const toBuffer = require('it-to-buffer')
const IPFS = require('ipfs')


describe('MFdb', async () => {

    let db: MFdb
    let ipfs

    let definition: DatabaseDefinition = {
        name: "test",
        tables: [
            {
                name: "player",
                columns: [
                    { name: "id", unique: true, primary: true },
                    { name: "name", unique: false },
                    { name: "currentTeam", unique: false },
                    { name: "battingHand", unique: false },
                    { name: "throwingHand", unique: false }
                ]
            }
        ]
    }

    //Used in test
    let firstCid
    let secondCid
    let thirdCid


    before('Before', async () => {

        ipfs = await IPFS.create({
            repo: './.tmp/test-repos/test'
        })

        db = new MFdb(ipfs)

        //Make sure to drop existing test database
        await db.dropDatabase("test")

    })

    it('should create an empty database', async () => {

        //Act
        await db.createDatabase(definition)

        //Assert
        try {
            let i=0
            await ipfs.files.stat(`/mfdb/test/`)
        } catch (ex) {
            assert.fail("Didn't create database")
        }


        //Check that schema exists in right place
        let buffer = await toBuffer(ipfs.files.read(`/mfdb/test/definition/${DefinitionService.DEFINITION_FILENAME}`))
        let readDefinition = JSON.parse(buffer.toString())
        assert.notStrictEqual(definition, readDefinition)

        //Get CID
        let cid = await db.getDatabaseCID('test')
        assert.strictEqual(cid.toString(), "QmektKwdwQA52fFRX9Di3gdY79LU7crKthwprko3eeXbgL")


    })

    it('should fail to create database that already exists', async () => {

        let error

        try {
            await db.createDatabase(definition)
        } catch (ex) {
            error = ex
        }

        if (!error) {
            assert.fail('Did not fail to create database')
        }

    })

    it('should fail to create database without primary key defined', async () => {

        let error

        try {
            await db.createDatabase({
                name: "test-fail-database",
                tables: [
                    {
                        name: "player",
                        columns: [
                            { name: "id", unique: true },
                            { name: "name", unique: false },
                            { name: "currentTeam", unique: false },
                            { name: "battingHand", unique: false },
                            { name: "throwingHand", unique: false }
                        ]
                    }
                ]
            })
        } catch (ex) {
            error = ex
        }

        if (!error) {
            assert.fail('Did not fail to create database with bad table')
        }

    })

    it('should put items in the table and retreive them by primary key', async () => {

        await db.useDatabase("test")

        db.startTransaction()

        //Arrange 
        db.insert("player", {
            id: 1,
            name: "Pat"
        })

        db.insert("player", {
            id: 2,
            name: "Bill"
        })

        db.insert("player", {
            id: 3,
            name: "Jim"
        })

        db.insert("player", {
            id: 4,
            name: "Susan"
        })

        await db.commit()

        //Act
        let pat = await db.get("player", 1)
        let bill = await db.get("player", 2)
        let jim = await db.get("player", 3)
        let susan = await db.get("player", 4)


        //Assert
        assert.equal(pat.name, "Pat")
        assert.equal(bill.name, "Bill")
        assert.equal(jim.name, "Jim")
        assert.equal(susan.name, "Susan")

        //Check the count
        let count = await db.count("player")
        assert.equal(count, 4)

        //Get CID
        firstCid = "QmcFB8zH5dnY3dLjCZ3sTXKK7BQKqNrztRENe7a6syWdPH"

        let cid = await db.getDatabaseCID('test')
        assert.strictEqual(cid.toString(), firstCid)

        //Get CID of record and make sure it matches
        let patCID = await db.getCID("player", 1)
        assert.strictEqual(patCID.toString(), "Qmbdn17jRfQPwpNhHJaaNmSNipJVzaonuA7HoSFSUawHEv")



    })

    it('should fail to insert records that already exist', async () => {

        let error

        try {
            db.startTransaction()

            //Arrange 
            db.insert("player", {
                id: 1,
                name: "Pat"
            })

            await db.commit()

        } catch (ex) {
            error = ex
        }

        if (!error) {
            assert.fail('Did not fail to create database')
        }
    })

    it('should insert 100 items and read', async () => {

        console.time('Putting 100 records in mfsstore')

        db.startTransaction()

        for (let i = 5; i < 105; i++) {
            db.insert("player", {
                id: i,
                name: `Pat${i}`
            })
        }

        await db.commit()

        console.timeEnd('Putting 100 records in mfsstore')

        //Check the count
        let count = await db.count("player")
        assert.equal(count, 104)

        //Act
        console.time('Reading 100 records mfsstore')
        for (let i = 5; i < 105; i++) {
            let value = await db.get("player", i)
            assert.equal(value.name, `Pat${i}`)
        }
        console.timeEnd('Reading 100 records mfsstore')

        //Get CID
        secondCid = "QmbvxeMhGuBPhLqFyNgZqi1dh7hWMySc7EVcGpQTpSNozo"
        let cid = await db.getDatabaseCID('test')
        assert.strictEqual(cid.toString(), secondCid)


    })

    it('should retreive values by secondary indexes', async () => {

        //Arrange 

        db.startTransaction()

        db.insert("player", {
            id: 1010,
            name: "Andrew McCutchen",
            currentTeam: "PIT",
            battingHand: "R",
            throwingHand: "R"
        })

        db.insert("player", {
            id: 1020,
            name: "Pedro Alvarez",
            currentTeam: "BAL",
            battingHand: "R",
            throwingHand: "R"
        })

        db.insert("player", {
            id: 1030,
            name: "Jordy Mercer",
            currentTeam: "PIT",
            battingHand: "L",
            throwingHand: "R"
        })


        db.insert("player", {
            id: 1040,
            name: "Doug Drabek",
            currentTeam: "BAL",
            battingHand: "L",
            throwingHand: "R"
        })

        await db.commit()

        //Act
        let teamPIT = await db.searchIndex({
            table: "player",
            index: "currentTeam",
            value: "PIT",
            sortDirection: SortDirection.DESC,
            offset: 0,
            limit: 100
        })

        let teamBAL = await db.searchIndex({
            table: "player",
            index: "currentTeam",
            value: "BAL",
            sortDirection: SortDirection.DESC,
            offset: 0,
            limit: 100
        })

        let battingR = await db.searchIndex({
            table: "player",
            index: "battingHand",
            value: "R",
            sortDirection: SortDirection.DESC,
            offset: 0,
            limit: 100
        })
        let battingL = await db.searchIndex({
            table: "player",
            index: "battingHand",
            value: "L",
            sortDirection: SortDirection.DESC,
            offset: 0,
            limit: 100
        })

        let throwingR = await db.searchIndex({
            table: "player",
            index: "throwingHand",
            value: "R",
            sortDirection: SortDirection.DESC,
            offset: 0,
            limit: 100
        })

        //Teams
        assert.equal(teamPIT[0].name, "Andrew McCutchen")
        assert.equal(teamPIT[1].name, "Jordy Mercer")

        assert.equal(teamBAL[0].name, "Pedro Alvarez")
        assert.equal(teamBAL[1].name, "Doug Drabek")

        //Batting
        assert.equal(battingR[0].name, "Andrew McCutchen")
        assert.equal(battingR[1].name, "Pedro Alvarez")

        assert.equal(battingL[0].name, "Jordy Mercer")
        assert.equal(battingL[1].name, "Doug Drabek")

        //Pitching
        assert.equal(throwingR[0].name, "Andrew McCutchen")
        assert.equal(throwingR[1].name, "Pedro Alvarez")
        assert.equal(throwingR[2].name, "Jordy Mercer")
        assert.equal(throwingR[3].name, "Doug Drabek")

        //Get CID
        let cid = await db.getDatabaseCID('test')
        assert.strictEqual(cid.toString(), "Qma6bTEWTTVg94szFjwWhy7HrhjCQBAtaKLYt876TpTtx5")


    })


    it('should sort ASC and DESC', async () => {

        //Act
        let throwingR = await db.searchIndex({
            table: "player",
            index: "throwingHand",
            value: "R",
            sortDirection: SortDirection.ASC,
            offset: 0,
            limit: 100
        })

        //Pitching
        assert.equal(throwingR[0].name, "Doug Drabek")
        assert.equal(throwingR[1].name, "Jordy Mercer")
        assert.equal(throwingR[2].name, "Pedro Alvarez")
        assert.equal(throwingR[3].name, "Andrew McCutchen")


    })




    it('should get a full list when calling searchIndex with no value', async () => {

        let results = await db.searchIndex({
            table: "player",
            index: "id",
            sortDirection: SortDirection.DESC,
            offset: 0,
            limit: 100
        })

        assert.equal(results.length, 100)

    })

    it('should search for the primary key index if no index passed', async () => {

        //Act
        let results = await db.searchIndex({
            table: "player",
            sortDirection: SortDirection.DESC,
            offset: 0,
            limit: 100
        })


        //Assert
        assert.equal(results.length, 100)



    })



    it('should fail to update a row that does not exist', async () => {

        let error

        try {

            //Act
            db.startTransaction()

            db.update("player", {
                id: 5011,
                name: "Andrew McCutchen",
                currentTeam: "PIT",
                battingHand: "L", //was R
                throwingHand: "R"
            })

            await db.commit()

        } catch (ex) {
            error = ex
        }

        if (!error) {
            assert.fail('Did not fail to create update row')
        }


    })

    it('should update a row from one secondary index to another', async () => {

        //Act
        db.startTransaction()

        db.update("player", {
            id: 1010,
            name: "Andrew McCutchen",
            currentTeam: "PIT",
            battingHand: "L", //was R
            throwingHand: "R"
        })

        await db.commit()

        //Assert
        let battingR = await db.searchIndex({
            table: "player",
            index: "battingHand",
            value: "R",
            sortDirection: SortDirection.DESC,
            offset: 0,
            limit: 100
        })
        let battingL = await db.searchIndex({
            table: "player",
            index: "battingHand",
            value: "L",
            sortDirection: SortDirection.DESC,
            offset: 0,
            limit: 100
        })


        assert.equal(battingR.length, 1)
        assert.equal(battingR[0].name != "Andrew McCutchen", true)

        assert.equal(battingL[0].name, "Andrew McCutchen")

        //Get CID
        let cid = await db.getDatabaseCID('test')
        assert.strictEqual(cid.toString(), "QmVCVNfoab5Jd4LYix2yUhmigEWLQthskXGJar28uvwHLk")

    })

    it('should delete a row and remove it from all secondary indexes', async () => {

        //Act
        db.startTransaction()
        db.delete("player", 1030)
        await db.commit()

        //Assert
        let jordy = await db.get("player", 1030)
        let teamPIT = await db.searchIndex({
            table: "player",
            index: "currentTeam",
            value: "PIT",
            sortDirection: SortDirection.DESC,
            offset: 0,
            limit: 100
        })
        let battingL = await db.searchIndex({
            table: "player",
            index: "battingHand",
            value: "L",
            sortDirection: SortDirection.DESC,
            offset: 0,
            limit: 100
        })
        let throwingR = await db.searchIndex({
            table: "player",
            index: "throwingHand",
            value: "R",
            sortDirection: SortDirection.DESC,
            offset: 0,
            limit: 100
        })


        assert.equal(jordy, undefined)
        assert.equal(teamPIT.filter(player => player.name == "Jordy Mercer").length, 0)
        assert.equal(battingL.filter(player => player.name == "Jordy Mercer").length, 0)
        assert.equal(throwingR.filter(player => player.name == "Jordy Mercer").length, 0)

        //Get CID
        thirdCid = "QmWPVVpXB66tLrWwtGLYiEaRrqEmZFWyyM9V1o1FenwJn5"
        let cid = await db.getDatabaseCID('test')
        assert.strictEqual(cid.toString(), thirdCid)

    })

    it('should put a new record', async () => {

        //Act
        db.startTransaction()
        db.put("player", {
            id: '10000',
            name: "Bob Vance",
            currentTeam: "MIA",
            battingHand: "L", 
            throwingHand: "L"
        })
        await db.commit()

        //Assert
        let fetched = await db.get("player", "10000")

        assert.strictEqual(fetched.id, "10000")
        assert.strictEqual(fetched.name, "Bob Vance")
        assert.strictEqual(fetched.currentTeam, "MIA")
        assert.strictEqual(fetched.battingHand, "L")
        assert.strictEqual(fetched.throwingHand, "L")


    })



    it('should put an existing record', async () => {

        //Act
        db.startTransaction()
        db.put("player", {
            id: '10000',
            name: "Bob Vance",
            currentTeam: "TEX",
            battingHand: "R", 
            throwingHand: "R"
        })
        await db.commit()

        //Assert
        let fetched = await db.get("player", "10000")

        assert.strictEqual(fetched.id, "10000")
        assert.strictEqual(fetched.name, "Bob Vance")
        assert.strictEqual(fetched.currentTeam, "TEX")
        assert.strictEqual(fetched.battingHand, "R")
        assert.strictEqual(fetched.throwingHand, "R")


    })


    it('should flush the entire database to MFS', async () => {

        //Act
        await db.flushToMFS()

        let cid = await db.getDatabaseCID('test')
        assert.strictEqual(cid.toString(), "Qman3Jh8B5u3xY6L7MPKqcBCKsAruDDDLvGCwuS6mg6ySp")

        //Do it a second time.
        await db.flushToMFS()

        cid = await db.getDatabaseCID('test')
        assert.strictEqual(cid.toString(), "Qman3Jh8B5u3xY6L7MPKqcBCKsAruDDDLvGCwuS6mg6ySp")
    })






    it('should load a database from cid', async () => {

        //Update to state from first test
        await db.updateFromCid("test", firstCid)

        let pat = await db.get("player", 1)
        let bill = await db.get("player", 2)
        let jim = await db.get("player", 3)
        let susan = await db.get("player", 4)
        let patNonExist = await db.get("player", 5)
        let player = await db.get("player", 1010)

        assert.equal(pat.name, "Pat")
        assert.equal(bill.name, "Bill")
        assert.equal(jim.name, "Jim")
        assert.equal(susan.name, "Susan")
        assert.equal(patNonExist, undefined)
        assert.equal(player, undefined)

        //Update to overwritten state
        await db.updateFromCid("test", secondCid)

        let pat1 = await db.get("player", 5)
        let pat2 = await db.get("player", 6)
        let pat3 = await db.get("player", 7)
        let pat4 = await db.get("player", 8)
        let player2 = await db.get("player", 1010)


        assert.equal(pat1.name, `Pat5`)
        assert.equal(pat2.name, `Pat6`)
        assert.equal(pat3.name, `Pat7`)
        assert.equal(pat4.name, `Pat8`)
        assert.equal(player2, undefined)


        //Update to most recent state
        await db.updateFromCid("test", thirdCid)

        let player3 = await db.get("player", 1010)

        assert.equal(player3.name, "Andrew McCutchen")

    })


    // it('should insert 10000 items and read', async () => {

    //     console.time('Putting 10000 records in mfsstore')

    //     db.startTransaction()

    //     for (let i = 2000; i < 12000; i++) {
    //         db.insert("player", {
    //             id: i,
    //             name: `Pat${i}`,
    //             battingHand: "L",
    //             throwingHand: "R",
    //             currentTeam: "Pirates"
    //         })
    //     }

    //     await db.commit()

    //     console.timeEnd('Putting 10000 records in mfsstore')

    //     //Check the count
    //     let count = await db.count("player")
    //     assert.equal(count, 10107)

    //     //Act
    //     console.time('Reading 10000 records mfsstore')
    //     for (let i = 2000; i < 12000; i++) {
    //         let value = await db.get("player", i)
    //         assert.equal(value.name, `Pat${i}`)
    //     }
    //     console.timeEnd('Reading 10000 records mfsstore')

    // })


    it('should drop a database', async () => {

        //Act
        await db.dropDatabase("test")

        //Check if folder is gone
        try {
            await ipfs.files.stat(`/mfdb/test/`)
            assert.fail("Didn't drop database")
        } catch (ex) { }

    })

})
