// import assert from 'assert'

// import { DatabaseDefinition, MFdb, SortDirection } from "../src/index"
// import { DefinitionService } from '../src/service/definition-service'

// const toBuffer = require('it-to-buffer')
// const IPFS = require('ipfs')


// describe('Benchmark', async () => {

//     let db: MFdb
//     let ipfs

//     let definition: DatabaseDefinition = {
//         name: "test",
//         tables: [
//             {
//                 name: "player",
//                 columns: [
//                     { name: "id", unique: true, primary: true },
//                     { name: "name", unique: false },
//                     { name: "currentTeam", unique: false },
//                     { name: "battingHand", unique: false },
//                     { name: "throwingHand", unique: false }
//                 ]
//             }
//         ]
//     }

//     //Used in test
//     let firstCid
//     let secondCid
//     let thirdCid


//     before('Before', async () => {

//         ipfs = await IPFS.create({
//             repo: './.tmp/test-repos/test'
//         })

//         db = new MFdb(ipfs)

//         //Make sure to drop existing test database
//         await db.dropDatabase("test")

//     })

//     it('should create an empty database', async () => {

//         //Act
//         await db.createDatabase(definition)

//         //Assert
//         try {
//             let i=0
//             await ipfs.files.stat(`/mfdb/test/`)
//         } catch (ex) {
//             assert.fail("Didn't create database")
//         }


//         //Check that schema exists in right place
//         let buffer = await toBuffer(ipfs.files.read(`/mfdb/test/definition/${DefinitionService.DEFINITION_FILENAME}`))
//         let readDefinition = JSON.parse(buffer.toString())
//         assert.notStrictEqual(definition, readDefinition)

//         //Get CID
//         let cid = await db.getDatabaseCID('test')
//         assert.strictEqual(cid.toString(), "QmektKwdwQA52fFRX9Di3gdY79LU7crKthwprko3eeXbgL")


//     })


//     it('should insert 100 items and read', async () => {

//         let numberOfRecords = 5000

//         console.time(`Putting ${numberOfRecords} records in database`)

//         db.startTransaction()

//         for (let i = 0; i < numberOfRecords; i++) {
//             db.insert("player", {
//                 id: i,
//                 name: `Pat${i}`,
//                 currentTeam: "BAL",
//                 battingHand: "L",
//                 throwingHand: "R"
//             })
//         }

//         await db.commit()

//         console.timeEnd(`Putting ${numberOfRecords} records in database`)

//         //Check the count
//         let count = await db.count("player")
//         assert.equal(count, 104)

//         //Act
//         console.time('Reading 100 records mfsstore')
//         for (let i = 5; i < 105; i++) {
//             let value = await db.get("player", i)
//             assert.equal(value.name, `Pat${i}`)
//         }
//         console.timeEnd('Reading 100 records mfsstore')

//         //Get CID
//         secondCid = "QmPTu4esVgy9ykCNoajY1jc3KALFKWLm6Bt7RWQfpHrUko"
//         let cid = await db.getDatabaseCID('test')
//         assert.strictEqual(cid.toString(), secondCid)


//     })


// })
